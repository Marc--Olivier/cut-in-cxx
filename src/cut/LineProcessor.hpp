#pragma once

#include <memory>
#include <string>

namespace cut {

class Ranges;

class LineProcessor {
public:
  virtual ~LineProcessor();
  virtual bool processLine(std::string &dst) = 0;
};

std::unique_ptr<LineProcessor> createIstreamAdaptor(std::istream &src);
std::unique_ptr<LineProcessor> createFileReader(const std::string &filePath);

std::unique_ptr<LineProcessor>
createOstreamAdaptor(std::unique_ptr<LineProcessor> src, std::ostream &writer);

std::unique_ptr<LineProcessor>
createFileWriter(std::unique_ptr<LineProcessor> src,
                 const std::string &filePath);

std::unique_ptr<LineProcessor>
createBytesExtractor(std::unique_ptr<LineProcessor> src,
                     const Ranges &activeRanges);
std::unique_ptr<LineProcessor>
createFieldsExtractor(std::unique_ptr<LineProcessor> src, const Ranges &fields,
                      char delimiter);

} // namespace cut
