#include "cut/CliParser.hpp"

#include <catch.hpp>

namespace cut {
namespace tests {

// TODO: tests should check that the created LineProcessors are correct

SCENARIO("CliParser.parse", "[CliParser]") {
  std::stringstream out;
  std::stringstream err;
  GIVEN("parser=CliParser(argc=2, argv={\"app\", "
        "\"--help\"}).setOut(out).setErr(err)") {
    const char *argv[] = {"app", "--help"};
    auto parser = CliParser(2, argv).setOut(out).setErr(err).checkPaths(false);
    WHEN("auto errorCode = parser.parse()") {
      auto errorCode = parser.parse();
      THEN("errorCode == 0") {
        INFO("out=" + out.str())
        INFO("err=" + err.str())
        CHECK(errorCode == 0);
      }
    }
  }
  GIVEN("parser=CliParser(argc=4, argv={\"app\", \"-b\", \"-2,3\", "
        "\"dummy.txt\"}).setOut(out).setErr(err)") {
    const char *argv[] = {"app", "-b", "-2,3", "dummy.txt"};
    auto parser = CliParser(4, argv).setOut(out).setErr(err).checkPaths(false);
    WHEN("auto lineProc = parser.parse()") {
      auto errorCode = parser.parse();
      THEN("parser.lineProc != nullptr and errorCode == 0") {
        INFO("out=" + out.str())
        INFO("err=" + err.str())
        CHECK(parser.lineProcessor() != nullptr);
        CHECK(errorCode == 0);
      }
    }
  }
  GIVEN("parser=CliParser(argc=4, argv={\"app\", \"-b\", \"-2,3\", "
        "\"dummy.txt\"}).setOut(out).setErr(err)") {
    const char *argv[] = {"app", "-b", "2", "dummy.txt", "bar.txt"};
    auto parser = CliParser(4, argv).setOut(out).setErr(err).checkPaths(false);
    WHEN("auto errorCode = parser.parse()") {
      auto errorCode = parser.parse();
      THEN("parser.lineProcessor == nullptr and errorCode == 0") {
        INFO("out=" + out.str())
        INFO("err=" + err.str())
        CHECK(parser.lineProcessor() != nullptr);
        CHECK(errorCode == 0);
      }
    }
  }
  GIVEN("parser=CliParser(argc=4, argv={\"app\", \"-f\", \"-2,3\", "
        "\"dummy.txt\"}).setOut(out).setErr(err)") {
    const char *argv[] = {"app", "-f", "-2,3", "dummy.txt"};
    auto parser = CliParser(4, argv).setOut(out).setErr(err).checkPaths(false);
    WHEN("auto lineProc = parser.parse()") {
      auto errorCode = parser.parse();
      THEN("parser.lineProc != nullptr and errorCode == 0") {
        INFO("out=" + out.str())
        INFO("err=" + err.str())
        CHECK(parser.lineProcessor() != nullptr);
        CHECK(errorCode == 0);
      }
    }
  }
  GIVEN("parser=CliParser(argc=5, argv={\"app\", \"-f\", \"-2,3\","
        "\"--complement\", \"dummy.txt\"}).setOut(out).setErr(err)") {
    const char *argv[] = {"app", "-f", "-2,3", "--complement", "dummy.txt"};
    auto parser = CliParser(5, argv).setOut(out).setErr(err).checkPaths(false);
    WHEN("auto lineProc = parser.parse()") {
      auto errorCode = parser.parse();
      THEN("parser.lineProc != nullptr and errorCode == 0") {
        INFO("out=" + out.str())
        INFO("err=" + err.str())
        CHECK(parser.lineProcessor() != nullptr);
        CHECK(errorCode == 0);
      }
    }
  }
}

} // namespace tests
} // namespace cut
