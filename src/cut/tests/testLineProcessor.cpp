#include "cut/LineProcessor.hpp"

#include "cut/Ranges.hpp"

#include <catch.hpp>
#include <sstream>

namespace cut {
namespace tests {

SCENARIO("IstreamAdaptor.processLine", "[LineProcessor][IstreamAdaptor]") {
  GIVEN("textStream=istringstream(\"Hello, World!\\nHello, Marc-Olivier!\") "
        "and \n"
        "reader=createIstreamAdaptor(textStream)") {
    auto textStream = std::istringstream("Hello, World!\nHello, Marc-Olivier!");
    auto reader = createIstreamAdaptor(textStream);
    WHEN("stop = reader->processLine(dst)") {
      std::string dst;
      auto stop = reader->processLine(dst);
      THEN("dst==\"Hello, World!\" and stop==false") {
        CHECK(dst == "Hello, World!");
        CHECK(stop == false);
      }
      AND_WHEN("stop = reader->processLine(dst)") {
        stop = reader->processLine(dst);
        THEN("dst==\"Hello, Marc-Olivier!\" and stop==true") {
          CHECK(dst == "Hello, Marc-Olivier!");
          CHECK(stop == true);
        }
      }
    }
  }
}

SCENARIO("OstreamAdaptor.processLine", "[LineProcessor][OstreamAdaptor]") {
  GIVEN("textStream=istringstream(\"Hello, World!\\nHello, Marc-Olivier!\") "
        "and \n"
        "reader=createIstreamAdaptor(textStream) and \n"
        "writer=stringstream() and \n"
        "writerAdaptor=createOstreamAdaptor(reader, writer") {
    auto textStream = std::istringstream("Hello, World!\nHello, Marc-Olivier!");
    auto writer = std::ostringstream();
    auto writerAdaptor =
        createOstreamAdaptor(createIstreamAdaptor(textStream), writer);
    WHEN("stop = writerAdaptor->processLine(dst)") {
      std::string dst;
      auto stop = writerAdaptor->processLine(dst);
      THEN("dst==\"Hello, World!\" and writer.str()==\"Hello, World!\" and "
           "stop==false") {
        CHECK(dst == "Hello, World!");
        CHECK(writer.str() == "Hello, World!");
        CHECK(stop == false);
      }
      AND_WHEN("stop = writerAdaptor->processLine(dst)") {
        stop = writerAdaptor->processLine(dst);
        THEN("dst==\"Hello, Marc-Olivier!\" and \n"
             "writer.str()==\"Hello, World!\\nHello, Marc-Olivier!\\n\" \n"
             "and stop==true") {
          CHECK(dst == "Hello, Marc-Olivier!");
          CHECK(writer.str() == "Hello, World!\nHello, Marc-Olivier!\n");
          CHECK(stop == true);
        }
      }
    }
  }
}

SCENARIO("BytesExtractor.processLine", "[LineProcessor][BytesExtractor]") {
  GIVEN("textStream=istringstream(\"1234567890\\nabcdefg\") and \n"
        "reader=createIstreamAdaptor(textStream) and \n"
        "bytesExtractor=createBytesExtractor(reader, Ranges::from({{1, 2}, {4, "
        "1}, {9, 1000}}))") {
    auto textStream = std::istringstream("1234567890\nabcdefg");
    auto bytesExtractor =
        createBytesExtractor(createIstreamAdaptor(textStream),
                             Ranges::from({{1, 2}, {4, 1}, {9, 1000}}));
    WHEN("stop = bytesExtractor->processLine(dst)") {
      std::string dst;
      auto stop = bytesExtractor->processLine(dst);
      THEN("dst==\"12490\" and stop==false") {
        CHECK(dst == "12490");
        CHECK(stop == false);
      }
      AND_WHEN("stop = bytesExtractor->processLine(dst)") {
        stop = bytesExtractor->processLine(dst);
        THEN("dst==\"abd\" and stop==true") {
          CHECK(dst == "abd");
          CHECK(stop == true);
        }
      }
    }
  }
}

SCENARIO("FieldsExtractor.processLine", "[LineProcessor][FieldsExtractor]") {
  GIVEN(
      "textStream=istringstream(\"1,2,3,4,5,6,7,8,9,0\\na,b,c,d,e,f,g\") and \n"
      "reader=createIstreamAdaptor(textStream) and \n"
      "fieldsExtractor=createBytesExtractor(reader, Ranges::from({{1, 2}, {4, "
      "1}, {9, 1000}}), ',')") {
    auto textStream = std::istringstream("1,2,3,4,5,6,7,8,9,0\na,b,c,d,e,f,g");
    auto fieldsExtractor =
        createFieldsExtractor(createIstreamAdaptor(textStream),
                              Ranges::from({{1, 2}, {4, 1}, {9, 1000}}), ',');
    WHEN("stop = fieldsExtractor->processLine(dst)") {
      std::string dst;
      auto stop = fieldsExtractor->processLine(dst);
      THEN("dst==\"1,2,4,9,0\" and stop==false") {
        CHECK(dst == "1,2,4,9,0");
        CHECK(stop == false);
      }
      AND_WHEN("stop = fieldsExtractor->processLine(dst)") {
        stop = fieldsExtractor->processLine(dst);
        THEN("dst==\"a,b,d\" and stop==true") {
          CHECK(dst == "a,b,d");
          CHECK(stop == true);
        }
      }
    }
  }
}

} // namespace tests
} // namespace cut
