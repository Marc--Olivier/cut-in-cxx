#include "cut/Ranges.hpp"

#include <catch.hpp>

namespace cut {
namespace tests {

using IVect = std::vector<size_t>;

SCENARIO("Ranges ctor", "[Ranges]") {
  GIVEN("starts={1, 3, 5} and lengths={1, 1, Ranges::MAX}") {
    auto starts = std::vector<size_t>{1, 3, 5};
    auto lengths = std::vector<size_t>{1, 1, Ranges::MAX};
    WHEN("ranges = Ranges(starts, lengths)") {
      auto ranges = Ranges(starts, lengths);
      THEN("ranges.starts() == starts and ranges.lengths() == lengths") {
        CHECK(ranges.starts() == starts);
        CHECK(ranges.lengths() == lengths);
      }
    }
  }
  GIVEN("None") {
    WHEN("The size of starts is diffferent from the size of lengths") {
      THEN("An std::invalid_argument is thrown") {
        CHECK_THROWS_AS(Ranges({1, 3, 5, 7}, {2, 3, 6}),
                        std::invalid_argument &);
        CHECK_THROWS_WITH(
            Ranges({1, 3, 5, 7}, {2, 3, 6}),
            "size of starts (4) should be the same as size of lengths (3)");
      }
    }
    WHEN("Ranges overlap") {
      THEN("An std::invalid_argument execption is thrown") {
        CHECK_THROWS_AS(Ranges({1, 3, 5}, {2, 1, 6}), std::invalid_argument &);
      }
    }
    WHEN("A range length is 0") {
      THEN("An std::invalid_argument execption is thrown") {
        CHECK_THROWS_AS(Ranges({1, 3, 5}, {0, 1, 6}), std::invalid_argument &);
      }
    }
  }
}

SCENARIO("Ranges.complement", "[Ranges]") {
  GIVEN("range = Ranges(starts={1, 6, 10} and lengths={2, 1, Ranges::MAX})") {
    auto starts = std::vector<size_t>{1, 6, 10};
    auto lengths = std::vector<size_t>{2, 1, maxLength(10)};
    auto ranges = Ranges(starts, lengths);
    WHEN("complement = ranges.complement()") {
      auto complement = ranges.complement();
      THEN("complement.starts() == {0, 3, 7} and \n"
           "complement.lengths() == {1, 3, 3}") {
        CHECK(complement.starts() == IVect({0, 3, 7}));
        CHECK(complement.lengths() == IVect({1, 3, 3}));
      }
    }
  }
  GIVEN("range = Ranges(starts={0, 6, 10} and lengths={2, 3, 5})") {
    auto starts = std::vector<size_t>{0, 6, 10};
    auto lengths = std::vector<size_t>{2, 3, 5};
    auto ranges = Ranges(starts, lengths);
    WHEN("auto complement = ranges.complement()") {
      auto complement = ranges.complement();
      THEN("complement.starts() == {2, 9, 15} and \n"
           "complement.lengths() == {4, 1, maxLength(15)}") {
        CHECK(complement.starts() == IVect({2, 9, 15}));
        CHECK(complement.lengths() == IVect({4, 1, maxLength(15)}));
      }
    }
  }
}

SCENARIO("RangesBuilder.addRange", "[Ranges]") {
  GIVEN("builder = RangesBuilder()") {
    auto builder = RangesBuilder();
    WHEN("builder.addRange(3, 2)") {
      builder.addRange(3, 2);
      THEN("builder.starts()=={3} and builder.lengths()=={2}}") {
        CHECK(builder.starts() == IVect({3}));
        CHECK(builder.lengths() == IVect({2}));
      }
    }
  }
  GIVEN("builder = RangesBuilder(starts={3, 10, 20}, lengths={3, 1, 2})") {
    auto builder = RangesBuilder({3, 10, 20}, {3, 1, 2});
    WHEN("builder.addRange(1, 1)") {
      builder.addRange(1, 1);
      THEN("builder.starts()=={1, 3, 10, 20} and builder.lengths()=={1, 3, 1, "
           "2}}") {
        CHECK(builder.starts() == IVect({1, 3, 10, 20}));
        CHECK(builder.lengths() == IVect({1, 3, 1, 2}));
      }
    }
    WHEN("builder.addRange(8, 1)") {
      builder.addRange(8, 1);
      THEN("builder.starts()=={3, 8, 10, 20} and builder.lengths()=={3, 1, 1, "
           "2}}") {
        CHECK(builder.starts() == IVect({3, 8, 10, 20}));
        CHECK(builder.lengths() == IVect({3, 1, 1, 2}));
      }
    }
    WHEN("builder.addRange(4, 1)") {
      builder.addRange(4, 1);
      THEN("builder.starts()=={3, 10, 20} and builder.lengths()=={3, 1, 2}}") {
        CHECK(builder.starts() == IVect({3, 10, 20}));
        CHECK(builder.lengths() == IVect({3, 1, 2}));
      }
    }
    WHEN("builder.addRange(6, 2)") {
      builder.addRange(6, 2);
      THEN("builder.starts()=={3, 10, 20} and builder.lengths()=={5, 1, 2}}") {
        CHECK(builder.starts() == IVect({3, 10, 20}));
        CHECK(builder.lengths() == IVect({5, 1, 2}));
      }
    }
    WHEN("builder.addRange(1, 2)") {
      builder.addRange(1, 2);
      THEN("builder.starts()=={1, 10, 20} and builder.lengths()=={5, 1, 2}}") {
        CHECK(builder.starts() == IVect({1, 10, 20}));
        CHECK(builder.lengths() == IVect({5, 1, 2}));
      }
    }
  }
}

//------------------------------------------------------------------------------

SCENARIO("splitStr", "[Utils]") {
  GIVEN("None") {
    WHEN("split = splitStr(\"-2,3,5-\", ',')") {
      auto split = splitStr("-2,3,5-", ',');
      THEN("split == {\"-2\", \"3\", \"5-\"}") {
        CHECK(split == StrVector({"-2", "3", "5-"}));
      }
    }
    WHEN("split = splitStr(\"-2\", '-')") {
      auto split = splitStr("-2", '-');
      THEN("split == {\"\",\"2\"}") { CHECK(split == StrVector({"", "2"})); }
    }
  }
}

SCENARIO("parseRange", "[Utils]") {
  GIVEN("None") {
    WHEN("range = parseRange(\"-2,4,6-8,10-\")") { // 1 based indices !!
      auto range = parseRange("-2,4,6-8,10-");
      THEN("range.starts() == {0, 3, 5, 9} and \n" // 0 based indices !!
           "range.lengths() == {1, 1, 3, maxLength(10)}") {
        CHECK(range.starts() == IVect({0, 3, 5, 9}));
        CHECK(range.lengths() == IVect({1, 1, 3, maxLength(10)}));
      }
    }
  }
}

} // namespace tests
} // namespace cut
