#include "cut/CliParser.hpp"

#include "cut/Ranges.hpp"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <iostream>

namespace cut {

namespace program_options = boost::program_options;

CliParser::CliParser(int argc, const char *argv[])
    : argc_(argc), argv_(argv), out_(&std::cout), err_(&std::cerr),
      checkPaths_(true), lineProcessor_(nullptr) {}

static std::string getInputFile(const program_options::variables_map &varMap,
                                const char *option) {
  if (varMap.count(option) == 0) {
    throw std::invalid_argument("No input file");
  }
  auto fileName = varMap[option].as<std::string>();
  auto filePath = boost::filesystem::path(fileName);
  if (filePath.is_relative()) {
    auto curPath = boost::filesystem::canonical(".");
    filePath = curPath / filePath;
  }
  if (!boost::filesystem::exists(filePath)) {
    throw std::invalid_argument("File " + filePath.string() +
                                " does not exist");
  }
  return boost::filesystem::canonical(filePath).string();
}

int CliParser::parse() {
  constexpr char BYTES_OPTION[] = "bytes=LIST";
  constexpr char COMPLEMENT_OPTION[] = "complement";
  constexpr char DELIMITER_OPTION[] = "delimiter=DELIM";
  constexpr char FIELDS_OPTION[] = "fields=LIST";
  constexpr char INPUT_FILE_OPTION[] = "input-file";

  // https://linux.die.net/man/1/cut
  program_options::options_description visibleCommands(
      "cut - remove sections from each line of files");
  visibleCommands.add_options()("bytes=LIST,b",
                                program_options::value<std::string>(),
                                "select only these bytes")(
      COMPLEMENT_OPTION,
      "complement the set of selected bytes, characters or fields")(
      "delimiter=DELIM,d", program_options::value<std::string>(),
      "use DELIM instead of TAB for field delimiter")(
      "fields=LIST,f", program_options::value<std::string>(),
      "select only these fields; also print any line that contains no "
      "delimiter character")("help,h", "display this help and exit");
  program_options::options_description hiddenCommands("Hidden options");
  hiddenCommands.add_options()(
      INPUT_FILE_OPTION, program_options::value<std::string>(), "input file");
  program_options::positional_options_description inputFilePositional;
  inputFilePositional.add(INPUT_FILE_OPTION, 1);

  program_options::options_description cmd{};
  cmd.add(visibleCommands).add(hiddenCommands);

  program_options::variables_map varMap;
  program_options::store(program_options::command_line_parser(argc_, argv_)
                             .options(cmd)
                             .positional(inputFilePositional)
                             .run(),
                         varMap);
  program_options::notify(varMap);

  if (varMap.count("help")) {
    out() << visibleCommands << '\n';
    return 0;
  }

  try {
    auto fileName = getInputFile(varMap, INPUT_FILE_OPTION);
    lineProcessor_ = createFileReader(fileName);
  } catch (std::exception &ex) {
    err() << "Error: " << ex.what() << std::endl;
    if (checkPaths_) {
      return -1;
    } else {
      lineProcessor_ = createIstreamAdaptor(std::cin);
    }
  }

  bool complement = (varMap.count(COMPLEMENT_OPTION) ? true : false);
  auto parseRangeAndComplement = [complement](const std::string &rangesStr) {
    auto ranges = parseRange(rangesStr);
    if (complement) {
      return ranges.complement();
    } else {
      return ranges;
    }
  };
  if (varMap.count(BYTES_OPTION) && varMap.count(FIELDS_OPTION)) {
    err() << "Error: cannot activate flags 'bytes' (-b) and 'fields' (-f) at "
             "the same time"
          << std::endl;
    return -2;
  }
  if (varMap.count(DELIMITER_OPTION) && !varMap.count(FIELDS_OPTION)) {
    err() << "Error: 'delimiter' option (-d) requires 'fields' (-f) option"
          << std::endl;
    return -3;
  }
  if (varMap.count(BYTES_OPTION)) {
    auto rangesStr = varMap[BYTES_OPTION].as<std::string>();
    auto ranges = parseRangeAndComplement(rangesStr);
    lineProcessor_ = createBytesExtractor(std::move(lineProcessor_), ranges);
  } else if (varMap.count(FIELDS_OPTION)) {
    auto delimterStr = (varMap.count(DELIMITER_OPTION)
                            ? varMap[DELIMITER_OPTION].as<std::string>()
                            : "\t");
    if (delimterStr.empty() || delimterStr.size() > 1) {
      err() << "Error: the delimiter '" + delimterStr +
                   "' should be one character"
            << std::endl;
      return -4;
    }
    auto rangesStr = varMap[FIELDS_OPTION].as<std::string>();
    auto ranges = parseRangeAndComplement(rangesStr);
    lineProcessor_ = createFieldsExtractor(std::move(lineProcessor_), ranges,
                                           delimterStr.at(0));
  }
  lineProcessor_ = createOstreamAdaptor(std::move(lineProcessor_), out());
  return 0;
}

} // namespace cut
