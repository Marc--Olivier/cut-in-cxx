#pragma once

#include <memory>
#include <vector>

namespace cut {

//------------------------------------------------------------------------------

class Ranges {
public:
  Ranges(std::vector<size_t> startIndices, std::vector<size_t> lengths);
  static Ranges
  from(const std::vector<std::pair<size_t, size_t>> &startsAndLengths);
  inline size_t size() const { return starts_.size(); }
  const auto &starts() const { return starts_; }
  const auto &lengths() const { return lengths_; }
  Ranges complement() const;

private:
  std::vector<size_t> starts_;
  std::vector<size_t> lengths_;

public:
  static const size_t MAX;
};

inline size_t maxLength(size_t start) { return Ranges::MAX - start - 1; }

//------------------------------------------------------------------------------

class RangesBuilder {
public:
  RangesBuilder();
  ~RangesBuilder();
  RangesBuilder(std::vector<size_t> starts, std::vector<size_t> lengths);
  Ranges build();

public:
  // WARNING: Indices are 1 based when manipulating the RangesBuilder
  // but 0 based when manipulated the Ranges
  RangesBuilder &addRange(size_t start, size_t length) {
    addRangePrivate(start, length);
    return *this;
  }
  RangesBuilder &addRangeFromStart(size_t end) {
    return addRange(MIN, end - MIN);
  }
  RangesBuilder &addRangeToEnd(size_t start) {
    return addRange(start, maxLength(start));
  }
  RangesBuilder &addPoint(size_t point) { return addRange(point, 1); }

public:
  std::vector<size_t> starts() const;
  std::vector<size_t> lengths() const;
  static constexpr const size_t MIN = 1;

private:
  void addRangePrivate(size_t start, size_t length);

private:
  std::unique_ptr<class RangesBuilderImpl> pImpl;
};

//------------------------------------------------------------------------------

using StrVector = std::vector<std::string>;
std::vector<std::string> splitStr(const std::string &text, char delimiter);

Ranges parseRange(const std::string &rangeString);

template <typename TContainer>
auto iteratorAt(TContainer &container, size_t at) {
  return container.begin() + static_cast<int>(at);
}

} // namespace cut
