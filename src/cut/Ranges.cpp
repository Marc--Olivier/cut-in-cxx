#include "cut/Ranges.hpp"

#include <boost/algorithm/string.hpp>
#include <cassert>
#include <sstream>

namespace cut {

const size_t Ranges::MAX = std::numeric_limits<size_t>::max();

static void
checkRangesStartIndicesAndLengths(const std::vector<size_t> &starts,
                                  const std::vector<size_t> &lengths) {
  if (starts.size() != lengths.size()) {
    throw std::invalid_argument("size of starts (" +
                                std::to_string(starts.size()) +
                                ") should be the same as size of lengths (" +
                                std::to_string(lengths.size()) + ")");
  }
  const auto count = starts.size();
  if (count == 0) {
    return;
  }
  for (size_t i = 0; i < count - 1; ++i) {
    if (lengths[i] == 0) {
      std::stringstream msg;
      msg << "lengths[" << i << "] = " << lengths[i] << " should not be 0"
          << std::endl;
      throw std::invalid_argument(msg.str());
    } else if (lengths[i] > maxLength(starts[i])) {
      std::stringstream msg;
      msg << "lengths[" << i << "] = " << lengths[i] << " should not be > "
          << maxLength(starts[i]) << std::endl;
      throw std::invalid_argument(msg.str());
    }
  }
  for (size_t i = 1; i < count; ++i) {
    if (starts[i - 1] + lengths[i - 1] >= starts[i]) {
      std::stringstream msg;
      msg << "start + length at(" << i - 1
          << ") = " << starts[i - 1] + lengths[i - 1] << " should be <= "
          << "start at(" << i << ") = " << starts[i] << std::endl;
      throw std::invalid_argument(msg.str());
    }
  }
}

Ranges::Ranges(std::vector<size_t> startIndices, std::vector<size_t> lengths)
    : starts_(std::move(startIndices)), lengths_(std::move(lengths)) {
  checkRangesStartIndicesAndLengths(starts_, lengths_);
}

Ranges
Ranges::from(const std::vector<std::pair<size_t, size_t>> &startsAndLengths) {
  RangesBuilder builder;
  for (const auto &startAndLength : startsAndLengths) {
    builder.addRange(startAndLength.first, startAndLength.second);
  }
  return builder.build();
}

Ranges Ranges::complement() const {
  std::vector<size_t> complStarts;
  std::vector<size_t> complLengths;
  if (starts_.empty()) {
    complStarts.push_back(0);
    complLengths.push_back(maxLength(0));
    return Ranges(complStarts, complLengths);
  }
  auto count = size();
  complStarts.reserve(count + 1);
  complLengths.reserve(count + 1);
  size_t prevValAfterEnd =
      (starts_.front() == 0 ? starts_.front() + lengths_.front() : 0);
  size_t startIndex = (starts_.front() == 0 ? 1 : 0);
  for (auto thisIndex = startIndex; thisIndex < count; ++thisIndex) {
    complStarts.push_back(prevValAfterEnd);
    auto thisStart = starts_.at(thisIndex);
    complLengths.push_back(thisStart - prevValAfterEnd);
    prevValAfterEnd = thisStart + lengths_.at(thisIndex);
  }
  if (prevValAfterEnd != MAX - 1) {
    complStarts.push_back(prevValAfterEnd);
    complLengths.push_back(maxLength(prevValAfterEnd));
  }
  return Ranges(complStarts, complLengths);
}

//------------------------------------------------------------------------------

struct Point {
  Point(size_t index_, bool isStart_) : index(index_), isStart(isStart_) {}
  size_t index;
  bool isStart;

  static bool isLessThan(const Point &p1, const Point &p2) {
    return p1.index < p2.index || (p1.index == p2.index && !p1.isStart);
  }
};

class RangesBuilderImpl {
public:
  RangesBuilderImpl(std::vector<Point> points) : points_(std::move(points)) {}

  std::vector<size_t> starts() const;
  std::vector<size_t> lengths() const;
  void addRange(size_t start, size_t length);

  static std::vector<Point> sortPoints(std::vector<size_t> starts,
                                       std::vector<size_t> lengths);
  static std::vector<Point> &removeSuperflousPoints(std::vector<Point> &points);

private:
  std::vector<Point> points_;
};

std::vector<Point> &
RangesBuilderImpl::removeSuperflousPoints(std::vector<Point> &points) {
  assert(points.front().isStart);
  assert(!points.back().isStart);
  for (size_t i = points.size() - 1; i > 0; --i) {
    bool isStart = points[i - 1].isStart;
    if (isStart == points[i].isStart) {
      assert(points[i - 1].index != points[i].index);
      if (isStart) {
        points.erase(iteratorAt(points, i), iteratorAt(points, i + 1));
      } else {
        points.erase(iteratorAt(points, i - 1), iteratorAt(points, i));
      }
    } else if (points[i - 1].index == points[i].index) {
      assert(!isStart);
      assert(points[i].isStart);
      points.erase(iteratorAt(points, i - 1), iteratorAt(points, i + 1));
    }
  }
  return points;
}

std::vector<Point> RangesBuilderImpl::sortPoints(std::vector<size_t> starts,
                                                 std::vector<size_t> lengths) {
  checkRangesStartIndicesAndLengths(starts, lengths);
  std::vector<Point> points;
  points.reserve(starts.size() * 2);
  for (size_t i = 0; i < starts.size(); ++i) {
    points.emplace_back(starts[i], true);
    points.emplace_back(starts[i] + lengths[i], false);
  }
  std::sort(points.begin(), points.end(), Point::isLessThan);
  return removeSuperflousPoints(points);
}

std::vector<size_t> RangesBuilderImpl::starts() const {
  assert(points_.size() % 2 == 0);
  std::vector<size_t> startPoints;
  startPoints.reserve(points_.size() / 2);
  for (size_t i = 0; i < points_.size(); i += 2) {
    assert(points_[i].isStart);
    assert(!points_[i + 1].isStart);
    startPoints.push_back(points_[i].index);
  }
  return startPoints;
}

std::vector<size_t> RangesBuilderImpl::lengths() const {
  assert(points_.size() % 2 == 0);
  std::vector<size_t> lengths;
  lengths.reserve(points_.size() / 2);
  for (size_t i = 0; i < points_.size(); i += 2) {
    assert(points_[i].isStart);
    assert(!points_[i + 1].isStart);
    lengths.push_back(points_[i + 1].index - points_[i].index);
  }
  return lengths;
}

static void checkStartAndLength(size_t start, size_t length) {
  if (start < RangesBuilder::MIN) {
    throw std::invalid_argument(
        "start (" + std::to_string(start) +
        ") should be >= " + std::to_string(RangesBuilder::MIN));
  }
  auto max = maxLength(start);
  if (length > max) {
    throw std::invalid_argument("length (" + std::to_string(length) +
                                ") should be < " + std::to_string(max));
  }
  if (length < 1) {
    throw std::invalid_argument("length (" + std::to_string(length) +
                                ") should be >= 1");
  }
}

void RangesBuilderImpl::addRange(size_t start, size_t length) {
  checkStartAndLength(start, length);
  Point startPoint{start, true};
  Point endPoint{start + length, false};

  auto itEnd = std::lower_bound(points_.begin(), points_.end(), endPoint,
                                Point::isLessThan);
  points_.insert(itEnd, endPoint);

  auto itStart = std::lower_bound(points_.begin(), points_.end(), startPoint,
                                  Point::isLessThan);
  points_.insert(itStart, startPoint);
  removeSuperflousPoints(points_);
}

RangesBuilder::RangesBuilder()
    : pImpl(new RangesBuilderImpl(std::vector<Point>{})) {}

RangesBuilder::RangesBuilder(std::vector<size_t> starts,
                             std::vector<size_t> lengths)
    : pImpl(new RangesBuilderImpl(
          RangesBuilderImpl::sortPoints(starts, lengths))) {}

RangesBuilder::~RangesBuilder() = default;

Ranges RangesBuilder::build() {
  auto starts = this->starts();
  for (auto &p : starts) {
    --p;
  }
  return Ranges(starts, lengths());
}

std::vector<size_t> RangesBuilder::starts() const { return pImpl->starts(); }
std::vector<size_t> RangesBuilder::lengths() const { return pImpl->lengths(); }

void RangesBuilder::addRangePrivate(size_t start, size_t length) {
  pImpl->addRange(start, length);
}

//------------------------------------------------------------------------------

// http://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/
StrVector splitStr(const std::string &text, char delimiter) {
  std::vector<std::string> results;
  boost::split(results, text, [delimiter](char c) { return c == delimiter; });
  return results;
}

[[noreturn]] static void throwRangeError(const std::string &rangeString) {
  throw std::invalid_argument("Invalid ranges: " + rangeString);
}

Ranges parseRange(const std::string &rangeString) {
  constexpr char groupChar = ',';
  constexpr char rangeChar = '-';
  RangesBuilder builder;
  auto mainSplit = splitStr(rangeString, groupChar);
  // TODO: handle errors when calling std::stoul
  for (const auto &group : mainSplit) {
    auto rangeStrVect = splitStr(group, rangeChar);
    if (rangeStrVect.empty()) {
      throwRangeError(rangeString);
    } else if (rangeStrVect.size() == 1) {
      auto val = std::stoul(rangeStrVect.front());
      builder.addPoint(val);
    } else if (rangeStrVect.size() != 2) {
      throwRangeError(rangeString);
    } else if (rangeStrVect.front().empty()) {
      auto endValue = std::stoul(rangeStrVect.back());
      builder.addRangeFromStart(endValue);
    } else if (rangeStrVect.back().empty()) {
      auto startValue = std::stoul(rangeStrVect.front());
      builder.addRangeToEnd(startValue);
    } else {
      auto startValue = std::stoul(rangeStrVect.front());
      auto endValue = std::stoul(rangeStrVect.back());
      if (endValue < startValue) {
        throwRangeError(rangeString);
      }
      builder.addRange(startValue, endValue - startValue + 1);
    }
  }
  return builder.build();
}

} // namespace cut
