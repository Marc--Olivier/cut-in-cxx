#include "cut/CliParser.hpp"

int main(int argc, const char *argv[]) {
  ::cut::CliParser cliParser(argc, argv);
  auto errorCode = cliParser.parse();
  if (errorCode != 0) {
    return errorCode;
  }
  auto lineProcessor = cliParser.lineProcessor();
  if (lineProcessor == nullptr) {
    return 0;
  }
  std::string buffer;
  bool stop = false;
  while (!stop) {
    stop = lineProcessor->processLine(buffer);
  }
  return 0;
}
