#include "cut/private/LineProcessorPrivate.hpp"

#include <cassert>
#include <iostream>

namespace cut {

//------------------------------------------------------------------------------
LineProcessor::~LineProcessor() = default;

std::unique_ptr<LineProcessor> createIstreamAdaptor(std::istream &src) {
  return std::unique_ptr<LineProcessor>(new details::IstreamAdaptor(src));
}

std::unique_ptr<LineProcessor> createFileReader(const std::string &filePath) {
  return std::unique_ptr<LineProcessor>(new details::FileReader(filePath));
}

std::unique_ptr<LineProcessor>
createBytesExtractor(std::unique_ptr<LineProcessor> src,
                     const Ranges &activeRanges) {
  return std::unique_ptr<LineProcessor>(
      new details::BytesExtractor(std::move(src), activeRanges));
}

std::unique_ptr<LineProcessor>
createFieldsExtractor(std::unique_ptr<LineProcessor> src, const Ranges &fields,
                      char delimiter) {
  return std::unique_ptr<LineProcessor>(
      new details::FieldsExtractor(std::move(src), fields, delimiter));
}

std::unique_ptr<LineProcessor>
createOstreamAdaptor(std::unique_ptr<LineProcessor> src, std::ostream &writer) {
  return std::unique_ptr<LineProcessor>(
      new details::OstreamAdaptor(std::move(src), writer));
}

std::unique_ptr<LineProcessor>
createFileWriter(std::unique_ptr<LineProcessor> src,
                 const std::string &filePath) {
  return std::unique_ptr<LineProcessor>(
      new details::FileWriter(std::move(src), filePath));
}

namespace details {

//------------------------------------------------------------------------------
IstreamAdaptor::IstreamAdaptor(std::istream &src) : src_(src) {}

bool IstreamAdaptor::processLine(std::string &dst) {
  std::getline(src_, dst);
  // Flags other than eof could be tested.
  // See http://en.cppreference.com/w/cpp/io/basic_ios/eof
  return src_.eof();
}

//------------------------------------------------------------------------------
FileReader::FileReader(const std::string &filePath) : IstreamAdaptor(reader_) {
  // https://stackoverflow.com/questions/17337602/how-to-get-error-message-when-ifstream-open-fails
  auto previousMask = reader_.exceptions();
  std::ios_base::iostate exceptionMask =
      previousMask | std::ios::failbit | std::ifstream::badbit;
  reader_.exceptions(exceptionMask);
  try {
    reader_.open(filePath);
  } catch (std::ios_base::failure &ex) {
    if (ex.code() == std::make_error_condition(std::io_errc::stream)) {
      throw std::invalid_argument("Stream error when trying to open " +
                                  filePath);
    } else {
      throw std::invalid_argument("Unknown failure when trying to open file " +
                                  filePath);
    }
  }
  reader_.exceptions(previousMask);
}

FileReader::~FileReader() = default;

//------------------------------------------------------------------------------
CombinedProcessor::CombinedProcessor(std::unique_ptr<LineProcessor> src)
    : src_(std::move(src)) {
  if (src_.get() == nullptr) {
    throw std::invalid_argument(
        "Error: CombinedProcessor created with nullptr");
  }
}

CombinedProcessor::~CombinedProcessor() = default;

//------------------------------------------------------------------------------
BytesExtractor::BytesExtractor(std::unique_ptr<LineProcessor> src,
                               const Ranges &keptBytes)
    : CombinedProcessor(std::move(src)), keptBytes_(keptBytes) {}

bool BytesExtractor::processLine(std::string &dst) {
  bool stop = inputLine(buffer_);
  auto inputSize = buffer_.size();
  dst.resize(inputSize);
  auto rangesCount = keptBytes_.size();
  const auto &starts = keptBytes_.starts();
  const auto &lengths = keptBytes_.lengths();
  size_t dstIndex = 0;
  for (size_t rangeIndex = 0; rangeIndex < rangesCount; ++rangeIndex) {
    auto start = starts[rangeIndex];
    if (start >= inputSize) {
      break;
    }
    auto length = lengths[rangeIndex];
    if (length == 1) {
      dst[dstIndex++] = buffer_[start];
      continue;
    }
    auto valAfterEnd = start + length;
    auto indexAfterMax = std::min(valAfterEnd, inputSize);
    std::copy(iteratorAt(buffer_, start), iteratorAt(buffer_, indexAfterMax),
              iteratorAt(dst, dstIndex));
    dstIndex += indexAfterMax - start;
  }
  assert(dstIndex <= inputSize);
  dst.resize(dstIndex);
  return stop;
}

//------------------------------------------------------------------------------
FieldsExtractor::FieldsExtractor(std::unique_ptr<LineProcessor> src,
                                 const Ranges &fields, char delimiter)
    : CombinedProcessor(std::move(src)), fields_(fields),
      delimiter_(delimiter) {}

bool FieldsExtractor::processLine(std::string &dst) {
  bool stop = inputLine(buffer_);
  auto splitLine = splitStr(buffer_, delimiter_);
  auto splitSize = splitLine.size();
  if (splitSize <= 1) {
    dst = buffer_;
    return stop;
  }
  dst.clear();
  dst.reserve(buffer_.size());
  auto rangesCount = fields_.size();
  const auto &starts = fields_.starts();
  const auto &lengths = fields_.lengths();
  for (size_t rangeIndex = 0; rangeIndex < rangesCount; ++rangeIndex) {
    auto start = starts[rangeIndex];
    if (start >= splitSize) {
      break;
    }
    auto length = lengths[rangeIndex];
    if (length == 1) {
      dst += splitLine.at(start) + delimiter_;
      continue;
    }
    auto valAfterEnd = start + length;
    auto indexAfterMax = std::min(valAfterEnd, splitSize);
    for (auto iBlock = start; iBlock < indexAfterMax; ++iBlock) {
      dst += splitLine.at(iBlock) + delimiter_;
    }
  }
  if (!dst.empty()) {
    // delete last delimiter
    dst.resize(dst.size() - 1);
  }
  return stop;
}

//------------------------------------------------------------------------------
OstreamAdaptor::OstreamAdaptor(std::unique_ptr<LineProcessor> src,
                               std::ostream &writer)
    : CombinedProcessor(std::move(src)), writer_(writer), isFirstLine_(true) {}

bool OstreamAdaptor::processLine(std::string &dst) {
  bool stop = inputLine(dst);
  if (isFirstLine_) {
    isFirstLine_ &= false;
  } else {
    writer_ << '\n';
  }
  writer_ << dst;
  if (stop) {
    // Ensure to finish the output with a new line
    if (!dst.empty() && dst.back() != '\n') {
      writer_ << '\n';
    }
    writer_ << std::flush;
  }
  return stop;
}

//------------------------------------------------------------------------------
FileWriter::FileWriter(std::unique_ptr<LineProcessor> src,
                       const std::string &filePath)
    : OstreamAdaptor(std::move(src), writer_) {
  // TODO: handle opening errors
  writer_.open(filePath);
}

FileWriter::~FileWriter() = default;

} // namespace details
} // namespace cut
