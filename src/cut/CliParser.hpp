#pragma once

#include "cut/LineProcessor.hpp"

namespace cut {

class CliParser {
public:
  CliParser(int argc, const char *argv[]);
  int parse();
  LineProcessor *lineProcessor() { return lineProcessor_.get(); }

public:
  CliParser &&setOut(std::ostream &out) {
    out_ = &out;
    return std::move(*this);
  }
  CliParser &&setErr(std::ostream &err) {
    err_ = &err;
    return std::move(*this);
  }
  CliParser &&checkPaths(bool checkPath) {
    checkPaths_ = checkPath;
    return std::move(*this);
  }

private:
  std::ostream &out() { return *out_; }
  std::ostream &err() { return *err_; }

private:
  int argc_;
  const char **argv_;
  std::ostream *out_;
  std::ostream *err_;
  bool checkPaths_;
  std::unique_ptr<LineProcessor> lineProcessor_;
};

} // namespace cut
