#pragma once

#include "cut/LineProcessor.hpp"
#include "cut/Ranges.hpp"

#include <fstream>

namespace cut {
namespace details {

//------------------------------------------------------------------------------
class IstreamAdaptor : public LineProcessor {
public:
  IstreamAdaptor(std::istream &src);
  bool processLine(std::string &dst) override;

private:
  std::istream &src_;
  std::string buffer;
};

//------------------------------------------------------------------------------
class FileReader : public IstreamAdaptor {
public:
  FileReader(const std::string &filePath);
  ~FileReader();

private:
  std::ifstream reader_;
};

//------------------------------------------------------------------------------
class CombinedProcessor : public LineProcessor {
public:
  CombinedProcessor(std::unique_ptr<LineProcessor> src);
  virtual ~CombinedProcessor();

protected:
  inline bool inputLine(std::string &dst) { return src_->processLine(dst); }

private:
  const std::unique_ptr<LineProcessor> src_;
};

//------------------------------------------------------------------------------
class BytesExtractor : public CombinedProcessor {
public:
  BytesExtractor(std::unique_ptr<LineProcessor> src, const Ranges &keptBytes);
  bool processLine(std::string &dst) override;

private:
  const Ranges keptBytes_;
  std::string buffer_;
};

//------------------------------------------------------------------------------
class FieldsExtractor : public CombinedProcessor {
public:
  FieldsExtractor(std::unique_ptr<LineProcessor> src, const Ranges &fields,
                  char delimiter);
  bool processLine(std::string &dst) override;

private:
  const Ranges fields_;
  const char delimiter_;
  std::string buffer_;
};

//------------------------------------------------------------------------------
class OstreamAdaptor : public CombinedProcessor {
public:
  OstreamAdaptor(std::unique_ptr<LineProcessor> src, std::ostream &writer);
  bool processLine(std::string &dst) override;

private:
  std::ostream &writer_;
  bool isFirstLine_;
};

//------------------------------------------------------------------------------
class FileWriter : public OstreamAdaptor {
public:
  FileWriter(std::unique_ptr<LineProcessor> src, const std::string &filePath);
  ~FileWriter();

private:
  std::ofstream writer_;
};

} // namespace details
} // namespace cut
