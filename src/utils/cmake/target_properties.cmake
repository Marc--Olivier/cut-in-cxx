option(CODE_COVERAGE "set to true to activate code coverage" OFF)

function(add_sanatizers target scope)
	if(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
		if(CMAKE_BUILD_TYPE STREQUAL "Debug")
			set(sanitizers "address")
			target_compile_options(${target}
				${scope} -fsanitize=${sanitizers},
			)
			target_link_libraries(${target}
				${scope} -fsanitize=${sanitizers},
			)
		endif()
	endif()
	if(sanitizers)
		message(STATUS "${target}: using ${sanitizers} sanitizers")
	endif()
endfunction()

function(setDefaultCompileOptions target)
	# Testing compiler: https://stackoverflow.com/questions/10046114/in-cmake-how-can-i-test-if-the-compiler-is-clang
	if(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
		list(APPEND compilerOptions -std=c++17 -Werror -Weverything -Wno-c++98-compat -Wno-padded)
		list(APPEND compilerOptions -DBOOST_NO_AUTO_PTR) # ensures that auto_ptr is not used...
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
		list(APPEND compilerOptions -std=c++1z -Werror -Wall)
		if(CODE_COVERAGE)
			list(APPEND compilerOptions --coverage)
			list(APPEND linkerOptions --coverage)
		endif()
	else()
		message(FATAL_ERROR "Compiler ${CMAKE_CXX_COMPILER_ID} is not supported")
	endif()
	message(STATUS "${target}: compiled with options: ${compilerOptions}")
	target_compile_options(${target} PUBLIC ${compilerOptions})
	if(linkerOptions)
		message(STATUS "${target}: linked with options: ${linkerOptions}")
		target_link_options(${target} PUBLIC ${linkerOptions})
	endif()
endfunction()

function(setDefaultTargetProperties target)
	setDefaultCompileOptions(${target})
	add_sanatizers(${target} PUBLIC)
endfunction()
