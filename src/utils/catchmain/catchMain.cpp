#define CATCH_CONFIG_MAIN
#include <catch.hpp>

// Generate main functions and therefore reduce
// the compilation time for each test project.
// See https://github.com/philsquared/Catch/blob/master/docs/slow-compiles.md
