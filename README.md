# README

[![pipeline status](https://gitlab.com/Marc--Olivier/cut-in-cxx/badges/master/pipeline.svg)](https://gitlab.com/Marc--Olivier/cut-in-cxx/commits/master)
&emsp;
[![coverage](https://gitlab.com/Marc--Olivier/cut-in-cxx/badges/master/coverage.svg?job=test-with-coverage)](https://gitlab.com/Marc--Olivier/cut-in-cxx/commits/master)

This repositiory contains a C++ version of `cut`, a small utility to process the
lines of a file and select fields or bytes for each of them. It is based on the
[Linux cut](https://linux.die.net/man/1/cut) utility.

```sh
cut - remove sections from each line of files:
  -b [ --bytes=LIST ] arg      select only these bytes
  --complement                 complement the set of selected bytes, characters
                               or fields
  -d [ --delimiter=DELIM ] arg use DELIM instead of TAB for field delimiter
  -f [ --fields=LIST ] arg     select only these fields; also print any line
                               that contains no delimiter character
  -h [ --help ]                display this help and exit
```

## Examples

#### Selecting bytes of a file

```sh
$ cat tests/01_testsBytes.txt
1234567890
abcdefghi

$ cut -b 1,3-5,10  tests/01_testBytes.txt
13450
acde

$ cut -b 1,3-5,10 --complement  tests/01_testBytes.txt
26789
bfghi
```

#### Selecting fields (columns) of a file

```sh
$ cat 02_testFields.txt
foo1	foo2	foo3	foo4	foo5
bar1	bar2	bar3
aaa1	aaa2	aaa3  aaa4

$ cut -f 3,5 tests/02_testFields.txt
foo3	foo5
bar3
aaa3

$ cut -f 3,5 --complement tests/02_testFields.txt
foo1	foo2	foo4
bar1	bar2
aaa1	aaa2	aaa4
```

#### Selecting fields of a files specifying the delimiter

```sh
$ cat tests/03_testFields.txt
foo1;foo2;foo3;foo4;foo5
bar1;bar2;bar3
aaa1;aaa2;aaa3;aaa4

$ cut -f 3,5 -d ';' tests/03_testSplit.txt
foo3;foo5
bar3
aaa3

$ cut -f 3,5 -d ';' --complement tests/03_testSplit.txt
foo1;foo2;foo4
bar1;bar2
aaa1;aaa2;aaa4
```

## Installation

#### Files structure of the project

```
root
  |
  #-- ci
  |
  #-- src
  |    |
  |    #-- cut
  |    |    |
  |    |    #-- private
  |    |    |
  |    |    #-- tests
  |    |
  |    #-- utils
  |         |
  |         #-- catchmain
  |         |     |
  |         |     #-- catch
  |         |
  |         #-- cmake
  |
  #-- tests
```

#### Compiling and installing

To compile, you need [CMake](https://cmake.org) version 3.7 or later.

You cand find below some commands to compile and install `cut` into the `root`
directory using the [Ninja](https://ninja-build.org) generator:

```sh
# export BOOST_ROOT=/Users/bouillotte/Documents/Data/02-dev/third_parties/build/osx/clang6/install
# export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$BOOST_ROOT/lib
$ export BOOST_ROOT=<your_boost_directory>
$ cd cut
$ mkdir -p build/release
$ mkdir install
$ cd build/release/
$ cmake -GNinja ../.. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../install
$ ninja
$ CTEST_OUTPUT_ON_FAILURE=true ninja test
$ ninja install
```

**Note**: running `cmake -GNinja ../../src` will fail if `cmake` cannot find
either the `Boost.Program_options` or `Boost.filesystem` library.

**Note**: running `ninja test` will fail if the directory that contains the
`Boost` shared libraries is not added to one of the following environment
variables:

- `PATH` on `Windows`
- `LD_LIBRARY_PATH` on `Linux`
- `DYLD_LIBRARY_PATH` on `macOS`

#### Supported compilers

The code is compiled by the following compilers in CI:

- clang-8.0.0
- gcc-7

If you experience any difficulty compiling `cut` with a different compiler or on
a different platform, please let me know.

#### Third party libraries

The code of the `cut` utility depends on:

- [Boost](http://www.boost.org)
  - [Boost.Program_options](http://www.boost.org/doc/libs/1_64_0/doc/html/program_options.html)
  - [Boost.Filesystem](http://www.boost.org/doc/libs/1_64_0/libs/filesystem/doc/index.htm)
- [Catch](https://github.com/philsquared/Catch)
  - `Catch` is a header files library, the header file `catch.hpp` has been
    copied to `utils/catchmain/catch`.

## Software architecture

#### Overview

`cut` code is composed of 3 main parts:

- `LineProcessor` is an interface to process text line by line.
- `CliParser` parses the command line arguments and produces a `LineProcessor`
  that will process the input file.
- `Ranges` and `RangesBuilder` take care of calculating ranges from the command
  line arguments and storing them for efficient line processing by classes
  implementing `LineProcessor`.

The `main` function creates a `LineProcessor` using `CliParser`, and process
lines until `LineProcessor` notifies that it has to stop.

#### Implementing LineProcessor

`LineProcessor` is implemented by several classes:

- `IstreamAdaptor`, reading lines of a `std::istream`.
- `FileReader`, opening a file and reading it line by line.
- `BytesExtractor` and `FieldsExtractor`, extracting the bytes and the fields
  respectively of a line provided by a source `LineProcessor`.
- `OstreamAdaptor`, writing the lines provided by a source `LineProcessor` into
  a `std::ostream`.

The different implementations of `LineProcessor` could be combined together in a
more complex way than what is required for the `cut` utility.

**Note**: instead of using a `LineProcessor` interface, the
[Curiously recurring template pattern](https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern)
could have been used. This may improve the runtime performances by removing
virtual calls to `LineProcessor.processLine` and even optimize (inline?) the
calls.

#### Ranges

The user being able to provide fields or bytes ranges that are not organized, a
complex algorithm has to be developed to reorganize them (merging, sorting).

This is done by the class `RangesBuilder`, especially the method
`RangesBuilder.addRangePrivate`. Once all these transformations are done, a
`Ranges` object is created by `RangesBuilder.build`, that contains the `start`
and `length` values for each range.

## Others

#### Formatting markdown files

I use [prettier](https://prettier.io) for formatting the markdown files such as
README.md:

```sh
prettier --prose-wrap=always --write *.md
```

To install prettier, you can use [npm](https://www.npmjs.com):

```sh
npm install -g prettier
```

#### Running clang tools

Some scripts contained in `ci/scripts` run some checks against the source code
using some `clang` tools. If you don't have `clang` install on your machine, use
the script `ci/scripts/run-within-clang-docker` to run the script within Docker.
Note that the path relative to the root directory must be provided, for example:

```sh
ci/scripts/run-within-clang-docker ci/scripts/clang-tidy
```

## TODO

- [ ] Compile using Visual Studio
- [ ] Benchmarking/profiling the code (e.g. using
      [google/benchmark](https://github.com/google/benchmark))
- [ ] Creating script files to run `cut` with correctly set environment
      variables (e.g. to find the shared libraries)
- [ ] Better handling errors when:
  - [ ] reading/writing files
  - [ ] parsing program arguments, especially ranges (e.g. if a range contains
        letters instead of numbers)
