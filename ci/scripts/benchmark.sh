#!/usr/bin/env bash

#
# Benchmark this cut and the standard cut.
#
# The script requires the variable `install_dir` to be set
# such that `${install_dir}/bin/cut` is the executable to benchmark
# against the standard cut.
#
# For the benchmark, the script creates the file <root>/tmp/file_with_$1_lines.txt
# where $1 is the number of lines the file contains.
#
# Usage:
#   install_dir=./install/gcc ./ci/scripts/run_benchmark <number_of_lines_divided_by_3>
#
# Example:
#   # generates the file <root>/tmp/file_with_1000000_lines.txt
#   # that contains 1_000_000 lines
#   install_dir=./install/gcc ./ci/scripts/run_benchmark 1000000
#
function benchmark() {
  set -eu

  line_count=$1

  local root_dir && root_dir="$(root_dir)"
  tmp_dir=$root_dir/tmp
  input_file=$tmp_dir/file_with_${line_count}_lines.txt

  function createFile() {
    mkdir -p "$tmp_dir"
    awk 'BEGIN {
    names_count = ARGC-2
    offset = 2
    for (line = 1; line <= ARGV[1]; ++line) {
      printf "%d\t%s\t%s\t%s\n", line, ARGV[(line%names_count)+offset],
        ARGV[(line+1)%names_count+offset], ARGV[(line+2)%names_count+offset]
    }
  }' "${line_count}" pierre paul jean >"$input_file"
  }

  if [ ! -f "$input_file" ]; then
    echo "$(date '+%Y-%m-%d %H:%M:%S') start creating file $input_file"
    createFile
    echo "$(date '+%Y-%m-%d %H:%M:%S') Done."
  fi

  cd "$install_dir"/bin >/dev/null
  echo "cd ${install_dir}/bin && ./cut -f 1-2,4 $input_file"
  time ./cut -f 1-2,4 "$input_file" >/dev/null
  cd - >/dev/null
  echo

  echo "cut -f 1-2,4 $input_file"
  time cut -f 1-2,4 "$input_file" >/dev/null
  echo
}
