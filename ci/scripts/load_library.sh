#!/usr/bin/env bash

# Script that loads other scripts of this library

function _absolute_path() {
  if [[ -f "$1" ]]; then
    cd "$(dirname "$1")" >/dev/null 2>&1 && echo "$(pwd)"/"$(basename "$1")"
  elif [[ -d "$1" ]]; then
    cd "$1" >/dev/null 2>&1 && pwd
  else
    echo >&2 "'$1' is not a file/directory (current directory: $PWD)"
    return 1
  fi
}

readonly scripts_dir="$(_absolute_path "$(dirname "${BASH_SOURCE[0]}")")"

function root_dir() {
  git rev-parse --show-toplevel
}

pushd "$scripts_dir" >/dev/null || exit 1
source benchmark.sh
source docker.sh
popd >/dev/null || true
