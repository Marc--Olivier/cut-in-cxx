#!/usr/bin/env bash

readonly docker_dir="$(root_dir)"/ci/docker
readonly docker_image_tag="$(date '+%Y%m%d-%H%M%S')"

function docker_tag() {
  local label="$1"
  echo "mandrez/$label:$docker_image_tag"
}

function docker_build_all() {
  docker build "$docker_dir" --no-cache
  docker_build_gcc
  docker_build_clang
}

function docker_build_ubuntu-base() {
  local image_name && image_name="$(docker_tag cut-ci-ubuntu)"
  docker build "$docker_dir" --target=ubuntu-base -t "$image_name"
  docker_disk_usage_summary "$image_name"
}

function docker_build_gcc() {
  local image_name && image_name="$(docker_tag cut-ci-gcc)"
  docker build "$docker_dir" --target=ubuntu-gcc -t "$image_name"
  docker_disk_usage_summary "$image_name"
}

function docker_build_clang() {
  local image_name && image_name="$(docker_tag cut-ci-clang)"
  docker build "$docker_dir" --target=ubuntu-clang -t "$image_name"
  docker_disk_usage_summary "$image_name"
}

function docker_disk_usage_summary() {
  local image_name="$1"
  local file
  if file=$(docker run -i --entrypoint=bash "$image_name" <<<"cat /disk_usage.log" 2>/dev/null); then
    echo ""
    _disk_usage_log_pretty_print "$file"
    echo ""
  fi
}

# Pretty print previously logged disk usage.
# Usage:
#   disk_usage_log_pretty_print <file_path>
function _disk_usage_log_pretty_print() {
  local file="$1"
  printf "%-27s | %6s | %8s | %8s |\\n" "Package name" "Size" "Total" "Duration"
  echo "----------------------------|--------|----------|----------|"
  local -i prev_disk_usage=0
  local -i prev_timestamp=0
  while IFS=, read -r package_name disk_usage timestamp; do
    local -i package_size="$((disk_usage - prev_disk_usage))"
    prev_disk_usage="$disk_usage"
    if [[ $prev_timestamp == 0 ]]; then
      prev_timestamp=$timestamp
    fi
    local -i duration="$((timestamp - prev_timestamp))"
    prev_timestamp="$timestamp"
    printf "%-27s | %5dM | %7dM | %7ds |\\n" \
      "$package_name" \
      "$((package_size / 1000))" \
      "$((disk_usage / 1000))" \
      "$duration"
  done <<<"$file"
}
