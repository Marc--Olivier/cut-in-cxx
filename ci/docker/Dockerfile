FROM ubuntu:18.04 AS ubuntu-base

SHELL ["/bin/bash", "-c"]

# Add /opt/bin to the path
RUN mkdir -p /opt/bin
ENV PATH "$PATH:/opt/bin"

# Create `/opt/bin/disk_usage` script
RUN \
  echo $'#!/usr/bin/env bash\n\
  \n\
  target_dir="${1:-/}" \n\
  du -s "$target_dir" 2>&1 | grep -v /proc/ | sed \'s#\t.*##g\'\n\
  ' > /opt/bin/disk_usage && \
  chmod +x /opt/bin/disk_usage && \
  cat /opt/bin/disk_usage && \
  disk_usage

# Create `/opt/bin/disk_usage_log` script
ENV DEFAULT_DISK_USAGE_LOG_FILE /disk_usage.log
RUN \
  echo $'#!/usr/bin/env bash\n\
  \n\
  label="$1" \n\
  target_dir="${2:-/}" \n\
  printf "%s,%s,%(%s)T\\n" "$label" "$(disk_usage "$target_dir")" | tee -a "$DEFAULT_DISK_USAGE_LOG_FILE" \n\
  '> /opt/bin/disk_usage_log && \
  chmod +x /opt/bin/disk_usage_log && \
  cat /opt/bin/disk_usage_log && \
  disk_usage_log "(*) ubuntu:18.04"

# Create `/opt/bin/install_packages` script
RUN \
  echo $'#!/usr/bin/env bash\n\
  \n\
  package_names=("$@")\n\
  \n\
  function install_package_() { \n\
  local package_name="$1" \n\
  apt-get install -y --no-install-recommends "$package_name" &&\n\
  disk_usage_log "$package_name" \n\
  } \n\
  \n\
  for package in "${package_names[@]}"; do \n\
  install_package_ "$package" || exit 1 \n\
  done' > /opt/bin/install_packages && \
  chmod +x /opt/bin/install_packages && \
  cat /opt/bin/install_packages

RUN \
  apt-get update && \
  disk_usage_log "apt-get update" && \
  install_packages \
  ca-certificates curl git gpg-agent \
  python3 python3-pip python3-setuptools \
  # Contains `add-apt-repository`
  software-properties-common && \
  pip3 install --upgrade pip --no-cache-dir && \
  disk_usage_log "pip3 upgraded"

# Install [conan](https://github.com/conan-io/conan)
RUN \
  pip3 install conan --no-cache-dir && \
  disk_usage_log "conan"

RUN \
  disk_usage_log "(*) ubuntu-base"

# ------------------------------------------------------------------------------

FROM ubuntu-base AS ubuntu-gcc

# Add repo that contains g++-9.
# See https://launchpad.net/~ubuntu-toolchain-r/+archive/ubuntu/test
RUN \
  add-apt-repository ppa:ubuntu-toolchain-r/test -y

RUN \
  apt-get update && \
  disk_usage_log "apt-get update" && \
  install_packages g++-9 lcov

# Install [fastcov](https://github.com/RPGillespie6/fastcov)
# Note that fastcov requires gcov version >= 9
RUN \
  pip3 install fastcov --no-cache-dir && \
  disk_usage_log "fastcov"

RUN \
  disk_usage_log "(*) ubuntu-gcc"

# ------------------------------------------------------------------------------

FROM ubuntu-base AS ubuntu-clang

# Add repo that contains clang-10.
# See https://apt.llvm.org
RUN \
  curl https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add && \
  add-apt-repository "deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-10 main"

ENV CLANG_VERSION 10
RUN \
  apt-get update && \
  disk_usage_log "apt-get update" && \
  install_packages \
  clang-"$CLANG_VERSION" \
  clang-format-"$CLANG_VERSION" \
  clang-tidy-"$CLANG_VERSION"

RUN \
  declare -i version_extension_size="(( ${#CLANG_VERSION} + 1 ))" && \
  ls /usr/bin/{clang,llvm}*-"$CLANG_VERSION" | while read -r program; do \
  unversioned_program=${program:0:-$version_extension_size} && \
  rm -f "$unversioned_program" && \
  ln -s "$program" "$unversioned_program"; \
  done

RUN \
  ln -s /usr/bin/python3 /usr/bin/python

RUN \
  disk_usage_log "(*) ubuntu-clang"
